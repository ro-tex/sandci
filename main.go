package main

import (
	"fmt"
	"os"
	"time"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func Handler(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	return events.APIGatewayProxyResponse{
		StatusCode: 200,
		Body:       "Hello world! " + time.Now().Format("2006-01-02 15:04:05"),
	}, nil
}

func main() {
	fmt.Printf("%s is %s. years old\n", os.Getenv("VERSION"), os.Getenv("SHA"))
	lambda.Start(Handler)
}
