module gitlab.com/ro-tex/sandci

go 1.12

require (
	github.com/aws/aws-lambda-go v1.11.1
	github.com/stretchr/objx v0.2.0 // indirect
)
